void main(List<String> arguments) {
// 1) Пользователь вводит порядковый номер пальца руки. Необходимо показать его название на экран.
// Finger = 1 ➞ “большой палец”

  int finger = 5;

  if (finger == 1) {
    print('first finger');
  } else if (finger == 2) {
    print('second finger');
  } else if (finger == 3) {
    print('third finger');
  } else if (finger == 4) {
    print('fourth finger');
  } else if (finger == 5) {
    print('fifth finger');
  } else {
    print('Invalid number');
  }
  ;

// 2) В переменной min лежит число от 0 до 59. Определите в какую четверть
//часа попадает это число (в первую, вторую, третью или четвертую).

  int min = 46;

  if (min >= 1 && min <= 15) {
    print('first quater');
  } else if (min >= 16 && min <= 30) {
    print('second quater');
  } else if (min >= 31 && min <= 45) {
    print('third quater');
  } else if (min >= 46 && min <= 59) {
    print('fourth quater');
  } else {
    print('error min');
  }
  ;

  // 3)Переменная lang может принимать 2 значения: ru; en;. Если она имеет
  // значение ru, то в переменную arr  запишем массив дней недели на русском языке,
  // а если имеет значение en; – то на английском.

  String lang = 'ru';
  List<String> arr = [];

  if (lang == 'ru') {
    print(arr = [
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
      'Воскресенье'
    ]);
  } else if (lang == 'en') {
    print(arr = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    ]);
  } else {
    print("Please, choice 'en' or 'ru' ");
  }
  ;

// 1)Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым
// символом этой строки является буква 'a'. Если это так - выведите 'да', в
//противном случае выведите 'нет'.

  String string = 'asaasasadsds';

  if (string.substring(0, 1) == 'a') {
    print('Yes');
  } else {
    print('No');
  }
  ;
/* вариант 2 
 String string = 'asaasasadsds';  
  
if (string.startWith('a')   вариант 2  
if (string[0]==('a')   вариант 3  

  { 
    print('Yes');
  } else {
    print('No');
  }
  ;


*/

// 2)Переменная num может принимать одно из значений: 1, 2, 3 или 4. Если она имеет
//  значение '1', то в переменную result запишем 'зима', если имеет
//  значение '2' – 'лето' и так далее.

// вариант 1
  int num = 4;
  String result = '';

  if (num == 1) {
    print(result = 'Winter');
  } else if (num == 2) {
    print(result = 'Spring');
  } else if (num == 3) {
    print(result = 'Summer');
  } else if (num == 4) {
    print(result = 'Autumn');
  }
  ;

// вариант 2

  int num2 = 4;
  String result2 = '';

  switch (num2) {
    case 1:
      {
        print(result2 = 'Winter');
      }
      break;
    case 2:
      {
        print(result2 = 'Spring');
      }
      break;
    case 3:
      {
        print(result2 = 'Summer');
      }
      break;
    case 4:
      {
        print(result2 = 'Autumn');
      }
      break;
    default:
      {
        print('error');
      }
  }
  ;

// 3)Если переменная a меньше нуля, то выведите 'Верно', иначе выведите 'Неверно'.
//  Проверьте работу скрипта при a, равном 1, 0, -3.

  int a = -3;

  if (a < 0) {
    print('Верно');
  } else {
    print('Не верно');
  }
  ;

// 4)Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется
// сумме вторых трех цифр. Если это так - выведите 'да', в противном случае выведите 'нет'.
  String aa = '11 2 3 3 2 1';
  List sumForA = aa.split(' ').map(int.parse).toList();

  var sum1 = sumForA[0] + sumForA[1] + sumForA[2];
  var sum2 = sumForA[3] + sumForA[4] + sumForA[5];

  if (sum1 == sum2) {
    print('true');
  } else
    (print('false'));

/* 

что мы сделали: split перебирает значения внутри String, после split ставим пробел,
потому что у нас после каждого числа в String стоит пробел. 
Метод map берет каждый элемент нашего String и превращает их в int значения.
А потом мы берем все эти интовые значения и превращаем в List, который делаем Print. 

вот так еще код будет работать 
String aa = '1 * 2 * 3 * 3 * 2 # 1';
  List sumForA = aa.split('*').map(int.parse).toList();

А вот так если сделаем, код уже не будет рабоать
String aa = '1 * 2 * 3 * 3 * 2 # 1';
  List sumForA = aa.split('*').map(int.parse).toList();

чтобы это исправить можно сделать замену в String не нужного нам символа на нужный 
String aa = '1*2*3*3*2#1';
    List sumForA = aa.replaceAll('#', '*').split('*').map(int.parse).toList();
мы # заменили на *

вариант 2 



Другой вариант, но не верный 
  String numberSum = ('123321');
  if ((numberSum[0] + numberSum[1] + numberSum[2]) ==
      (numberSum[3] + numberSum[4] + numberSum[5])) {
    print('Yes');
  } else {
    print('No');
  }
  ;
*/

// 5)Нужно написать условие для действий пешехода при различных сигналах светофора.
// Если сигнал красный, то надо стоять, иначе, если желтый - надо приготовиться,
// а иначе - можно идти.

  String light = 'red';

  if (light == 'red') {
    print('Stop');
  } else if (light == 'yellow') {
    print('Wait');
  } else {
    print('Go');
  }
}
