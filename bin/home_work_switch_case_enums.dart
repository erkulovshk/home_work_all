import 'dart:core';
import 'dart:io';

void main(List<String> arguments) {
/* В переменной day лежит какое-то число из интервала от 1 до 31. 
Определите в какую декаду месяца попадает это число (в первую, вторую или третью).
ввод должен быть через консоль. Остальные задачи ниже заключил в коммент. */
  print('Выберите дату месяца и сможете узнать декаду');
  int day = int.parse(stdin.readLineSync()!);
// ниже есть вариант через if
  switch (day) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
      print('вы выбрали ПЕРВУЮ декаду');
      break;
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
      print('вы выбрали ВТОРУЮ декаду');
      break;
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
    case 31:
      print('вы выбрали ТРЕТЬЮ декаду');
      break;
    default:
      print('выберите даты в диапозоне от 1 до 30');
  }
  ;

// через if
  // if (day >= 1 && day <= 10) {
  //   print('вы выбрали первую декаду');
  // } else if (day >= 11 && day <= 20) {
  //   print('вы выбрали вторую декаду');
  // } else if (day >= 21 && day <= 31) {
  //   print('вы выбрали третью декаду');
  // } else {
  //   print('выберите даты в диапозоне от 1 до 30');
  // }
  // ;

/*В переменной month лежит какоервала от 1 до 12. Определите в какую пору 
года попадает этот месяц (зима, лето, весна, осень). Ввод должен быть через консоль

  print('Выберите   месяц и сможете узнать время года');
  int month = int.parse(stdin.readLineSync()!);

// ниже есть вариант через if
  switch (month) {
    case 12:
    case 1:
    case 2:
      print('вы выбрали ЗИМУ');
      break;
    case 3:
    case 4:
    case 5:
      print('вы выбрали ВЕСНУ ');
      break;
    case 6:
    case 7:
    case 8:
      print('вы выбрали ЛЕТО ');
      break;
    case 9:
    case 10:
    case 11:
      print('вы выбрали ОСЕНЬ');
      break;
    default:
      print('выберите месяц в диапозоне от 1 до 12');
  }
  ;

// через if
  // if (month == 1 || month == 2 || month == 12) {
  //   print('вы выбрали ЗИМУ');
  // } else if (month == 3 || month == 4 || month == 5) {
  //   print('вы выбрали ВЕСНУ декаду');
  // } else if (month == 6 || month == 7 || month == 8) {
  //   print('вы выбрали ЛЕТО декаду');
  // } else if (month == 9 || month == 10 || month == 11) {
  //   print('вы выбрали ОСЕНЬ');
  // } else {
  //   print('выберите месяц в диапозоне от 1 до 12');
  // }
  // ;

*/

/*Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. 
Если это так - выведите 'да', в противном случае выведите 'нет'.
ответ: написал через if.
  String myStringA = 'sdsfsdfsdfsdf';
  List listA = myStringA.split('');
  String yes = 'Да, условие совпадает';
  String no = 'условие не совпадает';
  var firstLetter = listA[0];

  switch (firstLetter) {
    case 'a':
      print(yes);
      break;
    default:
      print(no);
  }
  ;
  // через if
  // if (firstLetter == 'a') {
  //   print(yes);
  // } else {
  //   print(no);
  // }
*/

/* Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или
 3. Если это так - выведите 'да', в противном случае выведите 'нет'. 
  print('Введите список чисел');
  String number = stdin.readLineSync()!;
  List listForNumber = number.split('');
  var numberForCaseAndIf = listForNumber[0];
  String yes = 'Да, условие совпадает';
  String no = 'условие не совпадает';

  switch (numberForCaseAndIf) {
    case '1':
      print(yes);
      break;
    case '2':
      print(yes);
      break;
    case '3':
      print(yes);
      break;
    default:
      print(no);
  }

  // if (numberForCaseAndIf == '1' ||
  //     numberForCaseAndIf == '2' ||
  //     numberForCaseAndIf == '3') {
  //   print(yes);
  // } else {
  //   print(no);
  // }

 */

/* Дана строка из 3-х цифр. Найдите сумму этих цифр. То есть сложите как числа 
первый символ строки, второй и третий. Ввод должен быть через консоль 

  print('Введиет список чисел');
  String someTypes = stdin.readLineSync()!;
  List listForSum = someTypes.split('').map(int.parse).toList();
  var sumResult = listForSum[0] + listForSum[1] + listForSum[2];
  print(sumResult);
 */

/* Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется 
сумме вторых трех цифр. Если это так - выведите 'да', в противном случае выведите 'нет'.
ввод должен быть через консоль  
  String yes = 'Да, условие совпадает';
  String no = 'условие не совпадает';

  print('Введиет список чисел');
  String someTypes2 = stdin.readLineSync()!;
  List listForSum2 = someTypes2.split('').map(int.parse).toList();

  var sum1 = listForSum2[0] + listForSum2[1] + listForSum2[2];
  var sum2 = listForSum2[3] + listForSum2[4] + listForSum2[5];

  if (sum1 == sum2) {
    print(yes);
  } else
    (print(no));
*/
}






/*
из классной работы
____________________
 

____________________
dart:async  это возможность запускать код несколькими потоками. Типо изоляторы.
Код 1 останавливается, код 2 работает. 
В смартфоне изоляты будут выделять доп память, для работы каждого кода. 

____________________
Рекурсия в быту - это если два зеркало навести друг против друга. 
В программировании - это вызов функции внутри самой себя.
Дадим определение рекурсивной функции- это функция, в теле которой оператор вызывает функцию,
которую содержит данный оператор. Самым распространенным примером рекурсии служит известная функция 
вычисления факториала factоr(). Факториал некоторого числа это произведение чисел от 1 до этого числа.
____________________

____________________


____________________
import 'dart:async';
import 'dart:io';

enum TraficlightTypes { green, yellow, red }

void main() async {
  print('Загорелся зеленый цвет, можно идти!');
  startTimer(7, TraficlightTypes.green);
  await Future.delayed(Duration(seconds: 7));
// когда здесь времени останется 3 сек - сработает 1-й if (начинай бежать)
  print('Загорелся желтый цвет, начинайте одумываться!');
  startTimer(7, TraficlightTypes.yellow);
  await Future.delayed(Duration(seconds: 7));
// когда здесь времени останется 2 сек - сработает 2-й if (Теперь точно пора тормозить)
  print('Загорелся красный цвет,  одумываться поздно, стойте!');
}

void startTimer(int seconds, TraficlightTypes types) async {
  Timer.periodic(
    Duration(seconds: 1),
    (timer) {
      print(seconds - timer.tick);
      if ((types == TraficlightTypes.green && (seconds - timer.tick) == 3)) {
        print('Начинай бежать');
      }
      if ((types == TraficlightTypes.yellow && (seconds - timer.tick) == 2)) {
        print('Теперь точно пора тормозить');
      }
      if ((seconds - timer.tick) <= 1) {
        timer.cancel();
      }
    },
  );
}
____________________
void main() async {
  startTimer(5);
}
/*Создал таймер, принимает инт значения. Имя переменной seconds 
Внутри Duration можно выбрать дни, секунды и т.д., я поставил секунды. Т.е. через каждую 1 секунду
будет срабатывать таймер. В  блоке {} после timer, я расписываю, чтобы система от моего seconds
отнимала timer.tick - это int, равный 1 сек. Его я обернул в принт ( (timer) { print(seconds - timer.tick)).
Далее я указываю условие if, внутри которого останавливаю timer, через вызов  timer.cancel(), 
после того, как величина от разности моей seconds и системной seconds будет равна или меньше 1.
*/
void startTimer(int seconds) async {
  Timer.periodic(
    Duration(seconds: 1),
    (timer) {
      print(seconds - timer.tick);
      if ((seconds - timer.tick) <= 1) {
        timer.cancel();
      }
    },
  );
}
____________________
import 'dart:async';
import 'dart:io';

void main() async {
  print('Загорелся зеленый цвет, можно идти!');
  startTimer(3);
  await Future.delayed(Duration(seconds: 5));
  // await говорит, чтобы работа далее подождала в промежутке 5 сек
  // т.е. у нас первый таймер завершится в объеме 3 сек, потом пройдет еще 2 сек 
  // и только затем будет принт насчет желтого цвета и начнется след таймер 
  print('Загорелся желтый цвет, начинайте одумываться!');
  startTimer(4);
  await Future.delayed(Duration(seconds: 6));

  print('Загорелся красный цвет,  одумываться поздно, стойте!');
  startTimer(6);
}

void startTimer(int seconds) async {
  Timer.periodic(
    Duration(seconds: 1),
    (timer) {
      print(seconds - timer.tick);
      if ((seconds - timer.tick) <= 1) {
        timer.cancel();
      }
    },
  );
}
____________________
import 'dart:async';
import 'dart:io';
// ВСЕ ЧТО НАПИСАЛ НИЖЕ НЕ СРАБОТАЛО ДЛЯ РЕШЕНИЯ ПРОБЛЕМЫ, КОГДА СРАЗУ ВЫЗЫВАЮТСЯ ВСЕ ПРИНТ, ВМЕСТО ТОГО, ЧТОБЫ ПООЧЕРЕДИ ВЫЗЫВАТЬСЯ ПОСЛЕ ИСТЕЧЕНИЯ ТАЙМЕРА

//  в oid main() async я написал async, чтобы как минимум работать с ней
// при создании функций. Также нам станет доступно ключевое слово await, т.е. подожди
void main() async {
  print('Загорелся зеленый цвет, можно идти!');
  await startTimer(2);

  print('Загорелся желтый цвет, начинайте одумываться!');
  await startTimer(4);

  print('Загорелся красный цвет,  одумываться! поздно');
  await startTimer(6);
}
// при назначении функции перед началом условия написал async,
// чтобы указать программе, что моя функция закончится обязательно, но
//  в настоящий момент я точно не знаю когда именно она закончится
// Например в начале функции я указал, что создается Future объект, т.е. надо ее полюбому подождать.

Future<void> startTimer(int seconds) async {
  Timer.periodic(
    Duration(seconds: 1),
    (timer) {
      print(seconds - timer.tick);
      if ((seconds - timer.tick) <= 1) {
        timer.cancel();
      }
    },
  );
}
____________________
import 'dart:io';

enum Companies {
  Megacom,
  Nurtelecom,
  Beeline,
  Saima,
  Megaline,
  Besmart,
  Aknet,
  Katel
}

void main(List<String> arguments) {
  for (var element in Companies.values) {
    print('${element.index + 1} - ${element.name}');
  }
  print('введите номер компании');

  late String choise;

  do {
    choise = stdin.readLineSync()!;
    // в if я поставил условие, чтобы выводить принт только в рамках имеющихся в enum индексов
    // если клиент указал номер, к. больше индексов, то принт нижний не сработает.
    if (int.parse(choise) <= Companies.values.length) {
      print(
          'вы выбрали компанию ${Companies.values[int.parse(choise) - 1].name}');
      // в этой записи - 1 помогает выбирать верно нужную компанию. Также через int.parse я вставляю в вызов имени компании тот индекс, к. я получил от ввода. А затем в конце указал namе, так как мне нужно только имя компании.
    } else {
      print('Компании с таким номером нету. \nВведите от 1 до 5');
    }
  } while (int.parse(choise) > Companies.values.length);

  print('Для получения подробной инфорации нажмите +');

  final String moreInfo = stdin.readLineSync()!;

  if (moreInfo == '+') {
    switch (choise) {
      case '1':
        print('Компания появилось в году № 1');
        break;
      case '2':
        print('Компания появилось в году № 2');
        break;
      case '3':
        print('Компания появилось в году № 3');
        break;
      case '4':
        print('Компания появилось в году № 4');
        break;
      case '5':
        print('Компания появилось в году № 5');
        break;
      default:
    }
  } else {
    print('Завершение программы');
  }
}
____________________
/*
Создать енамы с названиями компаний. 
Сделать принт всех компаний с нумерацией.
Определить какая компания была выбрана исходя из терминала, предложить
пользователю просмотреть подробную информацию о компании по нажатию на +.
Завершить работу терминала, если была нажата другая кнопка.
По завершению показа подробной информации предложить выбор других компаний.
*/
enum Companies { Megacom, Nurtelecom, Beeline, Saima, Megaline }

void main(List<String> arguments) {
  for (var element in Companies.values) {
    print('${element.index + 1} - ${element.name}');
  }
  print('введите номер компании');

  late String choise;

  do {
    choise = stdin.readLineSync()!;
    switch (choise) {
      case '1':
        print('вы выбрали компанию ${Companies.values[0].name}');
        break;
      case '2':
        print('вы выбрали компанию ${Companies.values[1].name}');
        break;
      case '3':
        print('вы выбрали компанию ${Companies.values[2].name}');
        break;
      case '4':
        print('вы выбрали компанию ${Companies.values[3].name}');
        break;
      case '5':
        print('вы выбрали компанию ${Companies.values[4].name}');
        break;
      default:
        print('Компании с таким номером нету. \nВведите от 1 до 5');
    }
  } while (int.parse(choise) > 5);

  print('Для получения подробной инфорации нажмите +');

  final String moreInfo = stdin.readLineSync()!;

  if (moreInfo == '+') {
    switch (choise) {
      case '1':
        print('Компания появилось в году № 1');
        break;
      case '2':
        print('Компания появилось в году № 2');
        break;
      case '3':
        print('Компания появилось в году № 3');
        break;
      case '4':
        print('Компания появилось в году № 4');
        break;
      case '5':
        print('Компания появилось в году № 5');
        break;
      default:
    }
  } else {
    print('Завершение программы');
  }
}

____________________
import 'dart:io';

enum Companies { Megacom, Nurtelecom, Beeline, Saima, Megaline }

void main(List<String> arguments) {
  for (var element in Companies.values) {
    print('${element.index + 1} - ${element.name}');
  }
}
//  +1 помог мне индексы начать с +1 
/* 

1 - Megacom
2 - Nurtelecom
3 - Beeline
4 - Saima
5 - Megaline
*/


____________________
// задача - принтить индекс значения enum, а значение мы передаем в свитч кейс
// а созданная нами функция должна изначально работать с нашим enum

void main(List<String> arguments) {
//  у enum я вызвал itsschool, поэтому получил для него в консоли
// подходящий кейс
  someFunction(Megacom.itsschool);

// У enum можно через values вызвать иные функции, например forEach


  Megacom.values.forEach((e) {
    print(e); // принтуем все элементы Megacom вместе с Megacom:
// пример: Megacom.itsschool
  });


// Если хотим принт только имени элемента внутри Megacom, то нужна вот такая запись:
  Megacom.values.forEach((e) {
    print(e.name); // e.name делает принт без Megacom:
    // пример: itsschool
  });

  Megacom.values.forEach((e) {
    print(e.name); // e.index принт только индексов элементов внутри Megacom

  });
 
  print(Megacom.values[0].name); // можно так тоже принт имени через индекс
}

enum Megacom { itsschool, megalab, razrabotka }

// наша функция принимает enum и в зависисмомсти от вызванного в будущем
// значения внутри enum { itsschool, megalab, razrabotka } будет возвращать
// нужный print для соответсвующего кейса
void someFunction(Megacom otdel) {
  switch (otdel) {
    case Megacom.itsschool:
      print('К нам прилетел itsschool');
      break;
    case Megacom.razrabotka:
      print('К нам прилетела razrabotka');
      break;
    case Megacom.megalab:
      print('К нам прилетел megalab');
      break;

    default: // если все что выше не сработает, то можем вот это "исключение" запустить
      print('не смогли определить ');
      break;
  }
}
____________________
// можно поставить String 
void main(List<String> arguments) {
  someFunction('ddddddd');
}

void someFunction(String myString) {
  switch (myString) {
    case 'qwe':
      print('К нам прилетела qwe');
      break;
    case 'asd':
      print('К нам прилетела двойка');
      break;
    case 'fdds':
      print('К нам прилетела тройка');
      break;

    default: // если все что выше не сработает, то можем вот это "исключение" запустить
      print('не смогли определить ');
      break;
  }
}
____________________
// пример работы функции, к. принимает число 
void main(List<String> arguments) {
  someFunction(50);
}

void someFunction(int index) {
  switch (index) {
    case 1:
      print('К нам прилетела единичка');
      break;
    case 2:
      print('К нам прилетела двойка');
      break;
    case 3:
      print('К нам прилетела тройка');
      break;

    default: 
    // если все что выше не сработает, то можем вот это "исключение" запустить
      print('не смогли определить число');
      break; // в default не обязательно вызывать break
  }
}

*/
