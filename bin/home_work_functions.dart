// Сделайте функцию, которая возвращает квадрат числа. Число передается параметром.
int funcSquare({required int a}) {
  int c = a * a;
  return c;
}

// Сделайте функцию, которая возвращает сумму двух чисел.
void funcSum({required int c, required int d}) {
  print(c + d);
}

// Сделайте функцию, которая отнимает от первого числа второе и делит на третье.
double funcMinusDivide({
  required e,
  required f,
  required g,
}) {
  return (e - f) / g;
}

// Напишите функцию, которая принимает целые минуты и преобразует их в секунды.
String funcMinuteToSecund({required int min}) =>
    '$min minute = ${min * 60} seconds';

// Есть массив, List array = [1, 3, 4, 5, 6]; возвратите его первый элемент
List k = [1, 3, 4, 5, 6];
funcArray({required List k}) {
  return k[0];
}

// Создайте bool переменную и напишите условие (if…else), выведите сообщение
// «Переменная имеет значение (значение вашей переменной)
String funcForBool({required z}) {
  if (z == true) {
    return ('значение z = $z');
  } else
    return ('значение z = $z');
}

// Создайте функцию, которая принимает число в качестве единственного
// аргумента и возвращает true, если оно меньше или равно нулю, в противном
// случае возвращает false.
funcForInt({required dynamic intP}) {
  if (intP is int) {
    if (intP <= 0) {
      return ('значение intP true');
    } else {
      return ('значение intP false');
    }
  } else
    'error';
}

// Дан массив с числами. Проверьте, есть ли в нем два одинаковых числа подряд.
// Если есть - выведите 'да', а если нет - выведите 'нет'.

funcForSimilarVal(List forMyList) {
  int myVariable = 0;
  bool isContains = false;
  forMyList.forEach((e) {
    if (myVariable == e) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Да');
  } else {
    print('Нет');
  }
}

void main() {
  print(funcSquare(a: 8));

  funcSum(c: 10, d: 80);

  print(funcMinusDivide(e: 50, f: 10, g: 2));

  print(funcMinuteToSecund(min: 3));

  print(funcArray(k: k));

  bool z = false;
  print(funcForBool(z: z));

  var intP = -20;
  // var intP = 20;
  // var intP = 0;
  print(funcForInt(intP: intP));

  List<int> myList = [1, 0, 2, 3, 4, 9, 5, 6, 7, 7, 0];
  funcForSimilarVal(myList); //принт для последнего задания
}

/*
_______________

Из классной работы 
 List<int> myList = [1, 0, 2, 3, 4, 9, 5, 6, 7, 7, 0];
  int myVariable = 0;
  bool isContains = false; // изначально для isContains значение false, и тогда
  // в forEach нам не придется писать условие else.

  myList.forEach((e) {
    if (myVariable == e) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Да');
  } else {
    print('Нет');
  }
_______________
Из классной работы 
List<int> myList = [1, 2, 3, 4, 5, 6, 7, 7];
  int myVariable = 0;

  myList.forEach((e) {
    if (myVariable == e) { // Здесь мы спрашиваем, равен ли myVariable ('0') значению 'e', к. равно '1'
      print('да'); // нет, не равно, поэтому идем далее
    } else {
      print('нет');
    }
    myVariable = e; // здесь мы приравниваем myVariable к значению 'e', к. к настоящему времени равно '1'
  });
  // перед последним циклом наш myVariable уже равен 7, а последний цикл дает е
  // значение '7', в итоге мы получаем 7 = 7 и выходит ответ 'Да' 
_______________

Из классной работы 
Данный код помогает ловить и проверять любые значения переменной на два 
условия:
1 - переменная bool?
2 - если bool, то какое именно значение: true, false? 

void main() {
  var aaa = false;
  print(funcAAA(aaa: aaa));
 
}

String funcAAA({required dynamic aaa}) {
  if (aaa is bool) {
    if (!aaa) {
      return ('значение aaa false');
    } else {
      return ('значение aaa true');
    }
  } else
    return 'error';
}
_______________

void main() {
  List<int> newList = List.generate(6, (index) => index);
  addFunction(newList);
}
// рекурсия - это цикличность  функции, когда она входе своего выполнения
// обращается к самой себе

int addFunction(List<int> newList) {
  if (newList.length <= 1) {
    return newList[0];
  } else {
    print(newList.sublist(1));
    return newList[0] + addFunction(newList.sublist(1));
  }
}

/* 
здесь мы объявляем Ф addFunction, к. в параметрах принимает массив 
интов newList.  Далее в блоке if else мы проверяем какая длина нашего массива. 
Если длина меньше или равна 1, мы возвращаем этот единственный объект.
Иначе мы вызываем принт и отрезаем от нашего массива одно значение.
Затем возвращаем наш первый элемент и плюсуем к нему наш изначальный массив
с одним отрезанным элементом. 
if (newList.length <= 1) {
    return newList[0];
    вот это условие, помогает нам не вернуть пустой массив без элемента

*/


_______________
void main() {
  List list = List.generate(100, (index) => index);
  print(list);
}

// генерация чисел от 1 до 100 с помощью generate List 

_______________

void main() {
  var list = ["Джеймс", "Патрик", "Мэтью", "Том"];

  print('«Пример анонимной функции»');

  list.forEach((item) {
    print('${list.indexOf(item)}: $item');
  });
}

В приведенном выше примере мы определили анонимную функцию с нетиповым 
элементом аргумента. Функция
вызывается для каждого элемента в списке и выводит строки с 
указанным значением индекса.


_______________


void main() {
  var c;
  c = someFunction(5, 100);
  print(c);
// то что выше можно записать еще вот так: print(someFunction(5, 100));
}

int someFunction(int a, int b) {
  return a * b; // пишем функцию, которая возвращает нам какое-либо значение

// код выше более кратко можно записать вот так:
// int someFunction(int a, int b) => return a * b;
}

_______________


void main() {
  someFunction(a: 50, c: 100, b: 80);
} // можем не по очереди вызывать и объявлять параметры, все равно порядок
// в функции сохранится

// ниже мы сделали наши параметры именнованными через использование required
void someFunction({required int a, required int b, required int c}) {
  print('Its a - $a');
  print('Its b - $b');
  print('Its c - $c');
}

_______________

enum Company{
	MICROSOFT, 	// 0
	APPLE,		  // 1
	GOOGLE     	// 2
}

void main(){
	print( getOwnerName( Company.MICROSOFT ) ); // Bill Gates
	print( getOwnerName( Company.APPLE ) );     // Steve Jobs
	print( getOwnerName( Company.GOOGLE ) );    // Sergey Brin
}

String getOwnerName(Company company){
	switch(company){
		case Company.MICROSOFT: return 'Bill Gates';
		case Company.APPLE:     return 'Steve Jobs';
		case Company.GOOGLE:    return 'Sergey Brin';
	}
}

_______________

Также можно пройтись циклом по значениям перечисления.
Для этого надо обратиться к массиву значений списка values.

enum Company{
    MICROSOFT,  // 0
    APPLE,      // 1
    GOOGLE      // 2
}

void main(){
    Company.values.forEach((company){
        print( company.index.toString() + ". " + getOwnerName( company ) );
    });
    // 0. Bill Gates
    // 1. Steve Jobs
    // 2. Sergey Brin
}

String getOwnerName(Company company){
    switch(company){
        case Company.MICROSOFT: return 'Bill Gates';
        case Company.APPLE:     return 'Steve Jobs';
        case Company.GOOGLE:    return 'Sergey Brin';
    }
}



*/
