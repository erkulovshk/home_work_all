// рассмотрим работу  миксинов mixin

import 'lesson_15_class_extends.dart';

void main() {
  FourWD privodSupper = FourWD();

  print(privodSupper.getWDLexus());
  // Ответ: FWD - это метод класса Lexus, к. есть в mixin

  //  попробуем с LIST работать

  List<String> wds = [privodSupper.getWDJeep(), privodSupper.getWDLexus()];

  print(wds);
}

class Car {}

class BMW {
  getWD() {
    return 'AWD';
  }
}

class Jeep {
  getWDJeep() {
    return 'FWDJeep';
  }
}

class Lexus {
  getWDLexus() {
    return 'FWDLexus';
  }
}

class Honda {
  getWDHonda() {
    return 'BWD';
  }
}

class FourWD extends Car with Lexus, Jeep {}

// --------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------

/*
// рассмотрим работу  миксинов mixin

void main() {
  CatsFamily murzik = CatsFamily(voice2: '');

// если вызвать метод без параметров у mixin класса, то метод вызовится тот,
// к. принадлежит последнему классу указанному при создании mixin класса
  print(murzik.voice());
  // Ответ:  fyrk  - метод принадлежит Tiger

// попробуем вызвать у экземпляра mixin класса  CatsFamily метод run класса Cat, к.
//  входит в mixin класс. 
  print(murzik.run());
  // Ответ: 'i am running'
}

class Animal {
  voice() {
    return 'rrrr';
  }
}

mixin Cat {
  voice() {
    return 'myau';
  }

  run() {
    return 'i am running';
  }
}

mixin Dog {
  voice() {
    return 'gaf';
  }
}

mixin Tiger {
  voice() {
    return 'fyrk';
  }
}

mixin Wolf {
  voice() {
    return 'Auuuuffff';
  }
}

//  здесь наследуем методы у нескольких классов. Последним стоит Tiger
class CatsFamily extends Animal with Cat, Tiger {
  String voice2;

  CatsFamily({required this.voice2});
}

 */

// --------------------------------------------------------------------------------------------------------

/*

// рассмотрим работу implements. Он выступает в качестве шаблона и мы обязаны
//  обязательно использовать каждый метод у имплементируемого родителя
//  т.е. если я не вызову ни один метод, то будет ошибка.

void main() {
  String number = '10';

  // делаем  int из стринг вариант 1
  print(int.parse(number));

  print('делаем  int из стринг вариант 2 ');
  print(number.toInt());

  print('делаем print toDouble ');
  print(number.toDouble());

  //  интересная запись if с подстановкой через использование двух ??
  // объявим переменную и положим текст
  String number2 = 'asas';

// так как при extension мы использовали tryParse - попробуй, а не Parse,
// то мы не получим ошибку, а получим null при попытке спарсить int из текста.
// через два знака вопроса (??) мы заложили: if null, печатай вот это. Программа продолжает работу.

  print(number2.toInt() ?? 'Здесь не число');

  // попробуем выделить скобками  550 и вместо +996  поставим 0
  String numberPhone = '+996550700700';
  print(numberPhone.changeNumber());
  // Ответ: 0(550)700700

  // попробуем показать имя из метода, если номер заданный в main совпадает с номером в методе
  print(numberPhone.getName());
  // ответ: Shabdanbek
}

extension ToInt on String {
  getName() {
    String shabdanbek = 'Shabdanbek';
    String shabdanbekNumber = '0(550)700700';
// +996550700700 из этого в main получаем 0(550)700700 через  вызов метода this.changeNumber
    if (shabdanbekNumber == this.changeNumber()) {
      return shabdanbek;
    } else {
      print(this);
    }
  }

  // создаем метод для - попробуем выделить скобками  550 и вместо +996  поставим 0
  changeNumber() {
    String a = replaceAll('+996', '0');
    // далее после 0 просим обернуть с 1 до 4 символы в скобки, а затем перечислить
    //  символы с 4 и до длины a
    return '0(${a.substring(1, 4)})${a.substring(4, a.length)}';

    //  для повторения return this.split(''); сначала можно положить в массив, и разделить каждый символ
    // получим [+, 9, 9, 6, 5, 5, 0, 7, 0, 0, 7, 0, 0] при   print(numberPhone.changeNumber());
  }

  toDouble() {
    return double.tryParse(this);
  }

// делаем  int из стринг вариант 2.
// С помощью extension мы переписали методы String,  и добавили туда свой метод toInt.
  toInt() {
    return int.tryParse(this);
  }
}

*/

// --------------------------------------------------------------------------------------------------------

/*

void main() {
  final subaru = Subaru();
  final bmw = Bmw();

  subaru.showInfoAboutPrivod();
  bmw.showInfoAboutPrivod();
}



class Car {
  showInfoAboutPrivod() {}
}

class Subaru implements Car {
  @override
  showInfoAboutPrivod() {
    print('Subaru has 4 drive privod');
  }
}

class Bmw implements Car {
  // @override
  // showInfoAboutPrivod() {
  //   print('Bmw has 2 drive privod');
  // }
}


*/
