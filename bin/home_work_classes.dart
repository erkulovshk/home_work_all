// ПРОСТО РАСКРЫВАЕШЬ В КАЖДОМ БЛОКЕ КОММЕНТЫ С ФЛЕШЕМ И КАЖДЫЙ БЛОК БУДЕТ РАБОТАТЬ,
// ТАК КАК В КАЖДОМ БЛОКЕ ЕСТЬ СВОЙ MAIN. ИЛИ  МОЖНО КАЖДЫЙ БЛОК КОПИРОВАТЬ В ОТДЕЛЬНЫЙ ФАЙЛ И ТАМ ЗАПУСКАТЬ

/*

// Читатели библиотеки

void main() {
  Reader firstPerson = Reader(
      name: 'Azamat',
      sertificate_number: 10,
      facultate: 'medical',
      birthday: '10.06.1990',
      number: 0550700700);

  final twoPerson = Reader(
      name: 'Timur',
      sertificate_number: 20,
      facultate: 'programmer',
      birthday: '10.06.1985',
      number: 0700500300);

  final book1 = Book(bookS: 'Dingo', author: 'Джони Депп');
  final book2 = Book(bookS: 'Funny day', author: 'Роберт');
  final book3 = Book(bookS: 'Sun day', author: 'Джим Керри');

  firstPerson.takeBook(count: 4);

  twoPerson.takeBookName(booksName: ['Приключения', 'Словарь', 'Энциклопедия']);

  twoPerson.returnBook(returnBookName: ['Путешествия', 'Романы', 'Стихи']);

  firstPerson.takeBookName(booksName: ['$book1']);
  firstPerson.showInfoAboutBooks([book1, book2, book3]);
  firstPerson.countForTakeBooks([book1, book2, book3]);
}

//  Вот эту часть задания не сделал: Разработать программу, в которой создается массив объектов   класса Reader.

// класс книги

class Book {
  String bookS;
  String author;

  Book({required this.bookS, required this.author});

  @override
  String toString() {
    return bookS;
  }
}

//  класс читатель
class Reader {
  String name;
  int sertificate_number;
  String facultate;
  String birthday;
  int number;

  Reader(
      {required this.name,
      required this.sertificate_number,
      required this.facultate,
      required this.birthday,
      required this.number});

// метод выводит количество взятых книг

  void takeBook({required int count}) {
    print('$name взял $count книги. ');
  }

// метод выводит названия возвращаемых книг

  void returnBook({required List returnBookName}) {
    print('$name вернул   книги:');
    returnBookName.forEach((element) {
      print(element);
    });
  }

//  Вариант 1. Метод выводит какие книги взяты, к. перечислены в массиве метода ( через forEach)

  void takeBookName({required List booksName}) {
    print('$name взял  книги:');
    booksName.forEach((element) {
      print(element);
    });
  }

// Вариант 2. Метод выводит какие книги взяты, к. считает из массива перечисленных нами книг при вызове метода.
// а в строке вывода списка книг с двух сторон скобки уберем через метод
  showInfoAboutBooks(List<Book> books) {
    print('$name взял книги: ${books.map((e) => e.bookS)}'
        .replaceAll('(', '')
        .replaceAll(')', ''));
  }

// метод выводит количество взятых книг, к. считает из массива перечисленных нами книг при вызове метода

  countForTakeBooks(List<Book> books) {
    print('$name взял ${books.length} книги');
  }
}


*/




/*
import 'package:collection/collection.dart';

// 1) Country -  класс имеет поле название, климат (метод должен принтовать название места и климат)

void main() {
  final KG = Country(name: 'Kyrgyzstan', climate: 'умеренный');
  Country KZ = Country(name: 'Казахстан', climate: 'там степи и ветра');

  print(KG.toString());
  print(KZ.toString());
}

class Country {
  String name;
  String climate;

  Country({required this.name, required this.climate});

  @override
  String toString() {
    return (' Страна: ${name}, климат - ${climate}.');
  }
}
*/

/*
import 'package:collection/collection.dart';

// Car -  имеет поле мощность, цвет, название, цена (метод должен принтовать характеристики машины)

void main() {
  Car tesla = Car(characters: ['320 hp', 'Red', 'Model S', 60000]);
  print(tesla.characters);
}

class Car {
  List<dynamic> characters;
  Car({required this.characters});

  List returnCharacters() {
    return characters;
  }
}

 */

 /* Класс Phone.
 
void main() {
  List<int> numbers = [103, 102, 101, 911, 112, 104];

  final Phone samsung =
      Phone(number: 0700500500, model: 'Galaxi s20', weight: 132.5);

  final Phone iPhone =
      Phone(number: 0905200200, model: 'ProMax 400', weight: 150.4);

  final Phone xiaomi =
      Phone(number: 0708404040, model: 'Redmi Note 9', weight: 205);

  samsung.getNumber();
  iPhone.getNumber();
  xiaomi.getNumber();

  samsung.getModel();
  iPhone.getModel();
  xiaomi.getModel();

  samsung.getWeight();
  iPhone.getWeight();
  xiaomi.getWeight();

  samsung.printAll();
  samsung.receiveCall(name: 'Shabdan');
  print(iPhone.sendMessage(numbers: [101, 102, 103, 104]));

  // Вызвать из конструктора с тремя параметрами конструктор с двумя.
}

class Phone {
  int number;
  String model;
  double weight;

// Добавить конструктор в класс Phone, который принимает на вход три параметра для инициализации
// переменных класса - number, model и weight.
  Phone({required this.number, required this.model, required this.weight});

// Добавить конструктор, который принимает на вход два параметра для инициализации
// переменных класса - number, model.
// не сделал

// Добавить конструктор без параметров.
// не сделал

// Изменить класс Phone в соответствии с концепцией JavaBean. Смотрите решение
// задачи в видео 1, видео 2, видео 3, видео 4, видео 5.
// не сделал

// Создать метод sendMessage с аргументами переменной длины. Данный метод принимает
//  на вход номера телефонов, которым будет отправлено сообщение. Метод выводит на
//   консоль номера этих телефонов.
  List sendMessage({required List<int> numbers}) {
    return numbers;
  }

  void receiveCall({required String name}) {
    print('Звонит $name');
  }

  void printAll() {
    print('$number $model $weight');
  }

  void getNumber() {
    print('$model number = $number');
  }

  void getModel() {
    print('$model model = $model');
  }

  void getWeight() {
    print('$model weight = $weight');
  }
}

   */

/*







Из классной работы 
________________________________
import 'package:collection/collection.dart';

 
void main() {
  final Samsung =
      Phone(name: 'Samsung', model: 'A3', number: 0550112233, weight: 350);
  final Nokia =
      Phone(name: 'Nokia', model: 'S3', number: 0550909090, weight: 250);
  Phone iPhone =
      Phone(name: 'iPhone', model: 'x3', number: 0771500500, weight: 600);
  Samsung.printParametrs();
  Nokia.printParametrs();
  iPhone.printParametrs();
  Nokia.receiveCall();
  Samsung.receiveCall();
  iPhone.receiveCall();
  Nokia.getNumber();
  Samsung.getNumber();
  iPhone.getNumber();
}

class Phone {
  String name;
  int number;
  String model;
  int weight;

  Phone(
      {required this.name,
      required this.model,
      required this.number,
      required this.weight});

  void printParametrs() {
    print('$name : $model, number: $number, weight: $weight');
  }

  void receiveCall() {
    print('Звонит $name');
  }

  void getNumber() {
    print('Номер $name $number');
  }
}

________________________________

________________________________

________________________________

________________________________
void main() {
  List<int> numbers = [103, 102, 101, 911, 112, 104];

  final Phone samsung =
      Phone(number: 0700500500, model: 'Galaxi s20', weight: 132.5);

  final Phone iPhone =
      Phone(number: 0905200200, model: 'ProMax 400', weight: 150.4);

  final Phone xiaomi =
      Phone(number: 0708404040, model: 'Redmi Note 9', weight: 205);
  print(iPhone.sendMessage(numbers: numbers));

  samsung.getNumber();
  iPhone.getNumber();
  xiaomi.getNumber();

  samsung.getModel();
  iPhone.getModel();
  xiaomi.getModel();

  samsung.getWeight();
  iPhone.getWeight();
  xiaomi.getWeight();

  samsung.printAll();

// вызываю метод и указываю параметр namе. Ответ в консоли: Звонит Shabdan
  samsung.call(name: 'Shabdan');
}

class Phone {
  int number;
  String model;
  double weight;

  Phone({required this.number, required this.model, required this.weight});
// создал метод, к. возвращает лист из номеров, к. указаны в значаении другой заранее объявленной
// переменной  list в области main.
  List sendMessage({required List<int> numbers}) {
    return numbers;
  }

// Добавить в класс Phone методы: receiveCall, имеет один параметр – имя звонящего.
// Выводит на консоль сообщение “Звонит {name}”.
// здесь в параметры заложил переменные namе, а в принте обращаюсь в ней.

  void call({required String name}) {
    print('Звонит $name');
  }

  void printAll() {
    print('$number $model $weight');
  }

  void getNumber() {
    print('$model number = $number');
  }

  void getModel() {
    print('$model model = $model');
  }

  void getWeight() {
    print('$model weight = $weight');
  }
}
________________________________
import 'package:collection/collection.dart';

// test save
void main() {
// задача  создать класс Car и внутри ЧЕРЕЗ LIST расписать,
// затем  внутри надо создать метод, который принтует характеристики car

  Car tesla = Car(characters: ['320 hp', 'Red', 'Model S', 60000]);

  print(tesla);
  // так я расписал в классе метод toString, то при вызове принта вывожу по шаблону данные, 
  // того объекта, который создал
}

class Car {
  // здесь создал лис характеристики 'characters'. Затем указал required, чтобы
  // при создании экземпляра Car система обязательно требовала внести характ-ки
  List<dynamic> characters;
  Car({required this.characters});

// Далее метод toString, чтобы отображать харак-ти
  @override
  String toString() {
    return 'Car (characters:  $characters)';
  }
/*
Другой вариант 

//  теперь у объекта могу вызвать метод returnCharacters
  print(tesla.returnCharacters());\
  // Ответ в консоли:
  // [320 hp, Red, Model S, 60000]

  // внутри класса создаю метод ТОЛЬКО для return characters
  List returnCharacters() {
    return characters;
  }
 */ 

/*
  вариант  3 
//  в мэйн вызвать метод у объекта ( принт не указываем, но принт будет). 
  tesla.returnCharacters();
 
//  внутри класса расписать void метод с готовым print 

  void returnCharacters() {
    print(characters);
  }
 */ 

}
________________________________
import 'package:collection/collection.dart';

void main() {
// задача  создать класс страна и внутри него название страны и климат страны,
// создать надо метод внутри, который принтует

  final KG = Country(name: 'Kyrgyzstan', climate: 'умеренный');
// здесь создал экземляры класса Country двумя способами
  Country KZ = Country(name: 'Казахстан', climate: 'там степи и ветра');

  print(KG.toString());
  print(KZ.toString());
}

class Country {
  String name;
  String climate;

  Country({required this.name, required this.climate});

// создаю метод String, чтобы расписать в нем формат вывода данных для экземпляров класса Country

  @override
  String toString() {
    return (' Страна: ${name}, климат - ${climate}.');
  }
}

_______________________________
 Human({
    required this.name,
    required this.sureName,
    required this.middleName,
    required this.age,
    required this.gender,
  });

//  создал функцию внутри класса Human, чтобы возвращать ФИО 
 String connectFio() {
    return '${name} ${sureName} ${middleName}';
  }

// теперь одной строкой я могу получить все данные
  print(somePerson.connectFio());

___________________________________
import 'dart:convert';

import 'package:collection/collection.dart';

void main() {
   

  final thirdPerson = Human(
    name: 'Misha',
    sureName: 'Mishev',
    middleName: 'Mishavich',
    age: 40,
    gender: 'male',
  );
  final thirdPerson2 = Human(
    name: 'Misha',
    sureName: 'Mishev',
    middleName: 'Mishavich',
    age: 40,
    gender: 'male',
  );

  print(thirdPerson2 == thirdPerson);
}

class Human {
  String name;
  String sureName;
  String middleName;
  int age;
  String gender;

  Human({
    required this.name,
    required this.sureName,
    required this.middleName,
    required this.age,
    required this.gender,
  });

  @override
  String toString() {
    return 'Human(name: $name, sureName: $sureName, middleName: $middleName, age: $age, gender: $gender)';
  }

//  после запуска методов ниже (методы я сгенерировал с помощью Data class generator) я могу сравнить мои объекты с одинаковыми значениями его элементов и получить рещзультат true, если бы методов ниже не было, то я получил бы false, так как пусть содержание разное, но объекты разные.
// print(thirdPerson2 == thirdPerson);
// true
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Human &&
        other.name == name &&
        other.sureName == sureName &&
        other.middleName == middleName &&
        other.age == age &&
        other.gender == gender;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        sureName.hashCode ^
        middleName.hashCode ^
        age.hashCode ^
        gender.hashCode;
  }
}
__________________
import 'dart:core';
import 'dart:io';

void main() {
  

  final twoPerson = Human(
      name: 'Ivan',
      sureName: 'Ivanov',
      middleName: 'Ivanich',
      age: 25,
      gender: 'male',
      childrensName: ['Aaa', 'Bbb']);

  print(twoPerson);
//  вызываю Ивана и получаю запись согласно прописанному мною методу toString внутри
// конструктор. Вывод принта:
//Human (Name: Ivan, surname: Ivanov, middleName: Ivanich, )
}

class Human {
  String name;
  String sureName;
  String middleName;
  int age;
  String gender;
  List<String> childrensName;

  Human({
    required this.name,
    required this.sureName,
    required this.middleName,
    required this.age,
    required this.gender,
    required this.childrensName,
  });

//   @override обозначает, что мы можем переписать заранее
// созданный метод to String для нашего класса

  @override
  String toString() {
    return 'Human (Name: $name, surname: $sureName, middleName: $middleName, )';
}
// выше мы ретерном переписали метод и указали точечно, что будет система выводить,
// когда мы вызовем экземпляр нашего Human
}

__________________
void main() {
// здесь создаю экземпляр класса Human
// при создании required будет подсказывать какой ввод к какому элементу относится
  final somePerson = Human(
      name: 'Azamat',
      sureName: 'Azamatov',
      middleName: 'Avich',
      age: 20,
      gender: 'male',
      childrensName: ['Aaa', 'Bbb']);

// здесь вызываю имя у только что созданного человека
  print(somePerson.name);
}

class Human {
  String name;
  String sureName;
  String middleName;
  int age;
  String gender;
  List<String> childrensName;

// это конструктор класса, теперь он перестал ругаться, далее можно с ним работать.
// Добавил required, чтобы не запутаться

  Human({
    required this.name,
    required this.sureName,
    required this.middleName,
    required this.age,
    required this.gender,
    required this.childrensName,
  });
}

__________________
void main() {
   

// здесь создаю экземпляр класса Human
  final somePerson =
      Human('Azamat', 'Azamatov', 'Avich', 20, 'male', ['Aaa', 'Bbb']);
// здесь вызываю имя у только что созданного человека 
  print(somePerson.name);
}

class Human {
  final String name;
  final String sureName;
  final String middleName;
  int age;
  String gender;
  List<String> childrensName;
// это конструктор класса, теперь он перестал ругаться, далее можно с ним работать.
  Human(this.name, this.sureName, this.middleName, this.age, this.gender,
      this.childrensName);
}

*/
