// Читатели библиотеки

void main() {
  Reader firstPerson = Reader(
      name: 'Azamat',
      sertificate_number: 10,
      facultate: 'medical',
      birthday: '10.06.1990',
      number: 0550700700);

  final twoPerson = Reader(
      name: 'Timur',
      sertificate_number: 20,
      facultate: 'programmer',
      birthday: '10.06.1985',
      number: 0700500300);

  final book1 = Book(bookS: 'Dingo', author: 'Джони Депп');
  final book2 = Book(bookS: 'Funny day', author: 'Роберт');
  final book3 = Book(bookS: 'Sun day', author: 'Джим Керри');

  firstPerson.takeBook(count: 4);

  twoPerson.takeBookName(booksName: ['Приключения', 'Словарь', 'Энциклопедия']);

  twoPerson.returnBook(returnBookName: ['Путешествия', 'Романы', 'Стихи']);

  firstPerson.takeBookName(booksName: ['$book1']);
  firstPerson.showInfoAboutBooks([book1, book2, book3]);
  firstPerson.countForTakeBooks([book1, book2, book3]);
}

//  Вот эту часть задания не сделал: Разработать программу, в которой создается массив объектов   класса Reader.

// класс книги

class Book {
  String bookS;
  String author;

  Book({required this.bookS, required this.author});

  @override
  String toString() {
    return bookS;
  }
}

//  класс читатель
class Reader {
  String name;
  int sertificate_number;
  String facultate;
  String birthday;
  int number;

  Reader(
      {required this.name,
      required this.sertificate_number,
      required this.facultate,
      required this.birthday,
      required this.number});

// метод выводит количество взятых книг

  void takeBook({required int count}) {
    print('$name взял $count книги. ');
  }

// метод выводит названия возвращаемых книг

  void returnBook({required List returnBookName}) {
    print('$name вернул   книги:');
    returnBookName.forEach((element) {
      print(element);
    });
  }

//  Вариант 1. Метод выводит какие книги взяты, к. перечислены в массиве метода ( через forEach)

  void takeBookName({required List booksName}) {
    print('$name взял  книги:');
    booksName.forEach((element) {
      print(element);
    });
  }

// Вариант 2. Метод выводит какие книги взяты, к. считает из массива перечисленных нами книг при вызове метода.
// а в строке вывода списка книг с двух сторон скобки уберем через метод
  showInfoAboutBooks(List<Book> books) {
    print('$name взял книги: ${books.map((e) => e.bookS)}'
        .replaceAll('(', '')
        .replaceAll(')', ''));
  }

// метод выводит количество взятых книг, к. считает из массива перечисленных нами книг при вызове метода

  countForTakeBooks(List<Book> books) {
    print('$name взял ${books.length} книги');
  }
}
