// 1 - Дан массив с числами. Проверьте, есть ли в нем два одинаковых числа не подряд. Если есть - выведите 'да', а если нет - выведите 'нет'.
funcForSimilarVal(List forMyList) {
  int myVariable = 0;
  bool isContains = false;
  forMyList.sort((a, b) => a.compareTo(b));
  forMyList.forEach((e) {
    if (myVariable == e) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Да');
  } else {
    print('Нет');
  }
}

// 2 - Дан массив с числами - List<int> list = [1, 2, 3, 4, 0, 5, 5, 7]; Напишите функцию и передайте ей массив в качестве параметра. Функция должна вернуть вам среднее арифметическое данного массива, вы должны получить - 3.375.
funcAverage(List forAverage) {
  dynamic value = 0;

  for (int val in forAverage) {
    value += val;
  }
  print(value / forAverage.length);
}

// 3 - Дан массив с числами - List<int> list = [1, 5, 3, 2, 2, 5, 6, 1, 2, 3];. Напишите функцию, которая вернет вам длинну этого массива - вы должны получить 10. Использовать lenght ЗАПРЕЩАЕТСЯ.
funcCount(List listCount) {
  var count = 0;
  for (int element in listCount) {
    count++;
  }
  print(count);
}

/*
4 - Дан массив со значениями - List<dynamic> list = [1, '2', true, 1, 4, false, 'qwerty'];. Напишите функцию, которая будет определять тип данных каждого элемента и закидывать значение в другой массив. Вы должны получить - [int, String, bool, int, int, bool, String].
*/
funcAddlist(List yourList) {
  List<Type> types = [];
  yourList.forEach((element) {
    types.add(element.runtimeType);
  });

  print(types);
}

void main() {
  List<int> myList = [1, 2, 3, 2, 4, 0, 5];
  funcForSimilarVal(myList); //вызов функции у 1-го задания

  List<int> myList2 = [1, 2, 3, 4, 0, 5, 5, 7];
  funcAverage(myList2); //вызов функции у 2-го задания

  List<int> list3 = [1, 5, 3, 2, 2, 5, 6, 1, 2, 3, 6, 1, 2, 3, 5];
  funcCount(list3); //вызов функции у 3-го задания

  List<dynamic> list4 = [1, '2', true, 1, 4, false, 'qwerty'];
  funcAddlist(list4); //вызов функции у 4-го задания
}

/*
_______________

_______________

_______________

_______________

Из классной работы 
 List<int> myList = [1, 0, 2, 3, 4, 9, 5, 6, 7, 7, 0];
  int myVariable = 0;
  bool isContains = false; // изначально для isContains значение false, и тогда
  // в forEach нам не придется писать условие else.

  myList.forEach((e) {
    if (myVariable == e) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Да');
  } else {
    print('Нет');
  }
_______________
Из классной работы 
List<int> myList = [1, 2, 3, 4, 5, 6, 7, 7];
  int myVariable = 0;

  myList.forEach((e) {
    if (myVariable == e) { // Здесь мы спрашиваем, равен ли myVariable ('0') значению 'e', к. равно '1'
      print('да'); // нет, не равно, поэтому идем далее
    } else {
      print('нет');
    }
    myVariable = e; // здесь мы приравниваем myVariable к значению 'e', к. к настоящему времени равно '1'
  });
  // перед последним циклом наш myVariable уже равен 7, а последний цикл дает е
  // значение '7', в итоге мы получаем 7 = 7 и выходит ответ 'Да' 
_______________

Из классной работы 
Данный код помогает ловить и проверять любые значения переменной на два 
условия:
1 - переменная bool?
2 - если bool, то какое именно значение: true, false? 

void main() {
  var aaa = false;
  print(funcAAA(aaa: aaa));
 
}

String funcAAA({required dynamic aaa}) {
  if (aaa is bool) {
    if (!aaa) {
      return ('значение aaa false');
    } else {
      return ('значение aaa true');
    }
  } else
    return 'error';
}
_______________

void main() {
  List<int> newList = List.generate(6, (index) => index);
  addFunction(newList);
}
// рекурсия - это цикличность  функции, когда она входе своего выполнения
// обращается к самой себе

int addFunction(List<int> newList) {
  if (newList.length <= 1) {
    return newList[0];
  } else {
    print(newList.sublist(1));
    return newList[0] + addFunction(newList.sublist(1));
  }
}

/* 
здесь мы объявляем Ф addFunction, к. в параметрах принимает массив 
интов newList.  Далее в блоке if else мы проверяем какая длина нашего массива. 
Если длина меньше или равна 1, мы возвращаем этот единственный объект.
Иначе мы вызываем принт и отрезаем от нашего массива одно значение.
Затем возвращаем наш первый элемент и плюсуем к нему наш изначальный массив
с одним отрезанным элементом. 
if (newList.length <= 1) {
    return newList[0];
    вот это условие, помогает нам не вернуть пустой массив без элемента

*/


_______________
void main() {
  List list = List.generate(100, (index) => index);
  print(list);
}

// генерация чисел от 1 до 100 с помощью generate List 

_______________

void main() {
  var list = ["Джеймс", "Патрик", "Мэтью", "Том"];

  print('«Пример анонимной функции»');

  list.forEach((item) {
    print('${list.indexOf(item)}: $item');
  });
}

В приведенном выше примере мы определили анонимную функцию с нетиповым 
элементом аргумента. Функция
вызывается для каждого элемента в списке и выводит строки с 
указанным значением индекса.


_______________


void main() {
  var c;
  c = someFunction(5, 100);
  print(c);
// то что выше можно записать еще вот так: print(someFunction(5, 100));
}

int someFunction(int a, int b) {
  return a * b; // пишем функцию, которая возвращает нам какое-либо значение

// код выше более кратко можно записать вот так:
// int someFunction(int a, int b) => return a * b;
}

_______________


void main() {
  someFunction(a: 50, c: 100, b: 80);
} // можем не по очереди вызывать и объявлять параметры, все равно порядок
// в функции сохранится

// ниже мы сделали наши параметры именнованными через использование required
void someFunction({required int a, required int b, required int c}) {
  print('Its a - $a');
  print('Its b - $b');
  print('Its c - $c');
}

_______________

enum Company{
	MICROSOFT, 	// 0
	APPLE,		  // 1
	GOOGLE     	// 2
}

void main(){
	print( getOwnerName( Company.MICROSOFT ) ); // Bill Gates
	print( getOwnerName( Company.APPLE ) );     // Steve Jobs
	print( getOwnerName( Company.GOOGLE ) );    // Sergey Brin
}

String getOwnerName(Company company){
	switch(company){
		case Company.MICROSOFT: return 'Bill Gates';
		case Company.APPLE:     return 'Steve Jobs';
		case Company.GOOGLE:    return 'Sergey Brin';
	}
}

_______________

Также можно пройтись циклом по значениям перечисления.
Для этого надо обратиться к массиву значений списка values.

enum Company{
    MICROSOFT,  // 0
    APPLE,      // 1
    GOOGLE      // 2
}

void main(){
    Company.values.forEach((company){
        print( company.index.toString() + ". " + getOwnerName( company ) );
    });
    // 0. Bill Gates
    // 1. Steve Jobs
    // 2. Sergey Brin
}

String getOwnerName(Company company){
    switch(company){
        case Company.MICROSOFT: return 'Bill Gates';
        case Company.APPLE:     return 'Steve Jobs';
        case Company.GOOGLE:    return 'Sergey Brin';
    }
}

*/
