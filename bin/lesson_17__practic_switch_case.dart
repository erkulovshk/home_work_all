import 'dart:io';

void main() {
  // задача вывести только четные элементы

  List a = [12, 3, 4, 5, 7, 123, 6, 8];
  List result = [];
  List result2 = [];

  for (int i = 0; i < a.length; i++) {
    // этот код вернет только true/false для каждой переменной  print(a[i].isOdd);
    // если здесь == заменить на != то мы получим нечетные if (a[i] % 2 != 0)
    if (a[i] % 2 == 0) {
      result.add(a[i]);
    }
  }
  print(' вариант 1 $result');

// вариант 2
// запись ниже j уже не индекс, а сам элемент
  for (int j in a) {
    if (j.isEven) {
      result2.add(j);
    }
  }

  print(' вариант 2 $result2');
}

//  ------------------------------------------------------------------------------------------

//  ------------------------------------------------------------------------------------------

/*

import 'dart:io';

void main() {
  Phone samsung = Phone(number: 2113132, model: 'sd-98', weight: 256);

  samsung.printInfo();
  samsung.reciveCall('saaaaaad');
  samsung.getNumber();
}

class Phone {
  int number;
  String model;
  int weight;

  Phone({required this.number, required this.model, required this.weight});

  printInfo() {
    print('$number $model $weight ');
  }

  reciveCall(String name) {
    print('Звонит $name, с номера ${getNumber()}');
  }

  getNumber() {
    return number;
  }
}


*/

//  ------------------------------------------------------------------------------------------

/*

import 'dart:io';

void main() {
  var a = '10qwqwq'.toInt();

  // если a равно NULL то мы принтуем текст
  print(a ?? 'Задаваемый набор содержит буквы');
}

extension ToInt on String {
  toInt() {
    return int.tryParse(this);
  }
}

*/

//  ------------------------------------------------------------------------------------------

/*

import 'dart:io';

void main() {
  var a = '10qwqwq'.toInt();
  print(a);
}

//  типа переписали для String его методы, путем добавления в список методов нового метода toInt
extension ToInt on String {
  toInt() {
    //если поставить  parse, то наш код сможет только жестко работать с цифрами в this,
    //если там буквы, то мы получим ошибку кода и по сути всего приложения
    //return int.parse(this);

    // если поставить tryParse, то в случае наличия в  this букв наряду с числами, то нам вернется NULL,
    // который мы можем обработать и вернуть например 10 или текст 'Задаваемый набор содержит буквы'

    if (int.tryParse(this) == null) {
      return 'Задаваемый набор содержит буквы'; // 10;
    } else
      return int.tryParse(this);
  }
}
  

*/

//  ------------------------------------------------------------------------------------------

/*

import 'dart:io';

void main() {
  int a = '10'.toInt();
  print(a.runtimeType);
}

//  типа переписали для String его методы, путем добавления в список методов нового метода toInt
extension ToInt on String {
  toInt() {
    return int.parse(this);
  }
}

*/

//  ------------------------------------------------------------------------------------------

/* 

import 'dart:io';

void main() {
/* Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется 
сумме вторых трех цифр. Если это так - выведите 'да', в противном случае выведите 'нет'.
ввод должен быть через консоль  */

  print(func2(func1('123456')));
}

//  решение классами

func1(String num) {
  bool isTrue = false;
  String nums = num;

  List nums2 = nums.split('');

  int firstNum =
      int.parse(nums2[0]) + int.parse(nums2[1]) + int.parse(nums2[2]);
  int secondNum =
      int.parse(nums2[3]) + int.parse(nums2[4]) + int.parse(nums2[5]);

  if (firstNum == secondNum) {
    isTrue = true;
  }
  return isTrue;
  // весь наш класс работает на то, чтобы вернуть isTrue со значением false или true
}

func2(bool a) {
  switch (a) {
    case true:
      return ('Да');

    case false:
      return ('Нет');

    default:
      return ('error');
  }
  // весь наш класс работает на то, чтобы вернуть текст исходы из значения итогового,
  // которое возвращает класс func1
}


*/

//  ------------------------------------------------------------------------------------------

/* 
import 'dart:io';

void main() {
/* Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется 
сумме вторых трех цифр. Если это так - выведите 'да', в противном случае выведите 'нет'.
ввод должен быть через консоль  */

//  решение миксом, но без классов 

  String nums = '123456';

  List nums2 = nums.split('');

  int firstNum =
      int.parse(nums2[0]) + int.parse(nums2[1]) + int.parse(nums2[2]);
  int secondNum =
      int.parse(nums2[3]) + int.parse(nums2[4]) + int.parse(nums2[5]);

  bool isTrue = false;

//  Здесь isTrue присваиваем значение true, если итог двух сумм равный 
// Если итог двух сумм не равный, значит наша isTrue также остается false. 
  if (firstNum == secondNum) {
    isTrue = true;
  }

  switch (isTrue) {
    case true:
      print('Да');
      break;

    case false:
      print('Нет');
      break;

    default:
      print('error');
  }

//  решение через ввод с консоли путем создания массива 

  // String yes = 'Да, условие совпадает';
  // String no = 'условие не совпадает';

  // print('Введиет список чисел');
  // String someTypes2 = stdin.readLineSync()!;
  // List listForSum2 = someTypes2.split('').map(int.parse).toList();

  // var sum1 = listForSum2[0] + listForSum2[1] + listForSum2[2];
  // var sum2 = listForSum2[3] + listForSum2[4] + listForSum2[5];

  // if (sum1 == sum2) {
  //   print(yes);
  // } else
  //   (print(no));
}

*/

//  ------------------------------------------------------------------------------------------

/* 

import 'dart:io';

void main() {
/* В переменной day лежит какое-то число из интервала от 1 до 31. 
Определите в какую декаду месяца попадает это число (в первую, вторую или третью).
ввод должен быть через консоль. Остальные задачи ниже заключил в коммент. */

//  вызов - решение через функцию

  print('введите число');

  // вариант без func2
  // print(funkDecade(int.parse(stdin.readLineSync()!)));

  /*
Вариант с func2. Функция2 ждет итог работы Функции1 и принимает ее результат в качестве параметра. 
А функция1 возвращает String сообщение о то, какая декада. А функция2 готовит ответ, исходя из того, 
какой String ответ она получила от Функции1. Например: мы ввели  21. Функция1 вернула нам текст 'Третья декада'.
Функция2 обработала в кейс и для текста 'Третья декада' вывела свой ответ: "Ваше число от 21 до 30"
  */
  print(func2(funkDecade(int.parse(stdin.readLineSync()!))));

  // решение через if else

  // print('Введите день месяца');
  // int number = int.parse(stdin.readLineSync()!);

  // if (number >= 1 && number <= 10) {
  //   print('Первая декада');
  // } else if (number >= 11 && number <= 20) {
  //   print('Вторая декада');
  // } else if (number >= 21 && number <= 31) {
  //   print('Вторая декада');
  // } else {
  //   print('неверный ввод');
  // }
}

//  решение через функцию. Перед функцией пишем то, что она возвращает. В нашем случае String
//  Важно: если написать в типе VOID и внутри не прописать RETURN, но наша функция ничего не будет возвращать

String funkDecade(int num) {
  if (num >= 1 && num <= 10) {
    return ('Первая декада');
  } else if (num >= 11 && num <= 20) {
    return ('Вторая декада');
  } else if (num >= 21 && num <= 31) {
    return ('Третья декада');
  } else {
    return ('неверный ввод');
  }
}

String func2(String a) {
  switch (a) {
    case 'Первая декада':
      return 'Ваше число от 0 до 10';

    case 'Вторая декада':
      return 'Ваше число от 11 до 20';

    case 'Третья декада':
      return 'Ваше число от 21 до 30';

    default:
      return 'error';
  }
}


*/
