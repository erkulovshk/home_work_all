import 'dart:io';

void main(List<String> arguments) {
  print(
      'Курс на сегодня: \nДоллар США - покупка 80, продажа 90; \nЕвро - покупка 100, продажа 110; \nРоссийский рубль - покупка 1, продажа 1.5 \nТенге - покупка 0.20, продажа 0.30,  \n ');
  print(
      '\n1) Хотите обменять другую валюту на сом! \n2) Хотите обменять сом на другую валюту! \nВвод:  ');
  var chooseOperation = stdin.readLineSync();

  print('Выберите валюту: \n USD \n EUR \n RUB \n KZT \nВвод: ');
  var myCurrency = stdin.readLineSync();

  if (chooseOperation == '1') {
    print('Сколько $myCurrency хотите продать? \nВвод: ');
  } else if (chooseOperation == '2') {
    print('Сколько $myCurrency хотите купить? \nВвод: ');
  } else {
    print('не верно выбрали, попробуйте провести операцию сначала');
  }
  ;
  var amountOfCurrency = stdin.readLineSync();

  dynamic a =
      0; // dynamic, чтобы я мог перезаписать либо результат, либо текст 'error'
  if (chooseOperation == '1' && myCurrency == 'USD') {
    a = int.parse(amountOfCurrency!) * 80;
  } else if (chooseOperation == '1' && myCurrency == 'EUR') {
    a = int.parse(amountOfCurrency!) * 100;
  } else if (chooseOperation == '1' && myCurrency == 'RUB') {
    a = int.parse(amountOfCurrency!) * 1;
  } else if (chooseOperation == '1' && myCurrency == 'KZT') {
    a = int.parse(amountOfCurrency!) * 0.20;
  } // далее клиент покупает валюту, а обменное бюро получается продает валюту
  else if (chooseOperation == '2' && myCurrency == 'USD') {
    a = int.parse(amountOfCurrency!) * 90;
  } else if (chooseOperation == '2' && myCurrency == 'EUR') {
    a = int.parse(amountOfCurrency!) * 110;
  } else if (chooseOperation == '2' && myCurrency == 'RUB') {
    a = int.parse(amountOfCurrency!) * 1.5;
  } else if (chooseOperation == '2' && myCurrency == 'KZT') {
    a = int.parse(amountOfCurrency!) * 0.30;
  } else {
    a = 'error';
  }
  print('Обмен $a сом на $amountOfCurrency $myCurrency');
}

/*
классные работы и заметки 
пример работы простого калькулятора 

вариант 1 

 print('введите число 1');
  var firstNum = stdin.readLineSync(); // здесь делаем ввод числа 1
  print('введите число 2');

  var secondNum = stdin.readLineSync(); // здесь делаем ввод числа 2
  print(' +  или -');

  var sym = stdin.readLineSync(); // здесь делаем ввод - или +
  __________________________________________________________________

  dynamic a = 0; // dynamic, чтобы я мог перезаписать либо результат, либо текст 'error'
  if (sym == '-') {
    a = int.parse(firstNum!) - int.parse(secondNum!); // здесь наши числа в формате St
    a = int.parse(firstNum!) - int.parse(secondNum!); // >>> парсим в формат int
  } else {
    a = 'error';
  }
  print(a);

вариант 2 
 
  int a =0; 
  if (sym == '-') {
    a = int.parse(firstNum!) - int.parse(secondNum!);  
    } else if (sym == '+') {
    print(int.parse(firstNum!) + int.parse(secondNum!));
  } else {
    print('error');
  }
  if (a != 0) print(a);


  вариант 3
 

  if (sym == '-') {
    print(int.parse(firstNum!) - int.parse(secondNum!));
     } else if (sym == '+') {
    print(int.parse(firstNum!) + int.parse(secondNum!));
  } else {
    print('error');
  }



*/
