//  классы - это одна из 4-х парадигм ООП
// ?
//  что такое класс?
// что такое объект?
//
// свойства класса - Свойства классов - это по сути переменные внутри классов,
// в которые можно записывать данные (но в основном их используют для задания констант).

import 'dart:io';
import 'dart:math';

void main() {
// пример работы random, где тоже есть наследование. Наша переменная берет метод
// nextInt у системного класса Random. Этот код понадобится при решении задачи по отгадыванию случайного числа
  print('введите ваше число');
  var random = Random();
  var randomNum = random.nextInt(100);

  var a = int.parse(stdin.readLineSync()!);
  if (a < randomNum) {
    print('3');
  } else if (a > randomNum) {
    print('2');
  } else if (a == randomNum) {
    print('1111');
  }

  Nexia nexia1 = Nexia('sedan', 1.6, 4);

  Bmw e34 = Bmw('sedan', 4.4, 4, 1996, 'alphina', 400);
  print(e34.carsYear());
}

abstract class Car {
  String body;
  double v;

  Car({
    required this.body,
    required this.v,
  });

  returnDoor();
  returnGaz() {
    return v / 100;
  }
}

class Nexia extends Car {
  int doors;

  @override
  returnDoor() {
    return doors;
  }

  Nexia(String body, double v, this.doors) : super(body: body, v: v);
}

class Bmw extends Car {
  String model;
  int hp;
  int year;
  int doors;

// функция, к. возвращает возврат машины
  @override
  int carsYear() {
    return 2022 - year;
  }

  @override
  returnDoor() {
    return doors;
  }

  Bmw(String body, double v, this.doors, this.year, this.model, this.hp)
      : super(body: body, v: v);
}

/////////////////////////////////////////////////////////////////////////////////
/*
//  можно сделать Human абстрактыми. В абстрактных классах можно создать функции пустышки.
// НО у абстрактного класса нельзя создать ЭКЗЕМПЛЯР класса
// К примеру, можно создать класс абстрактный АВТО, а потом его параметры использовать при создании
//  реальных машин с конкретными моделями и марками

abstract class Human {
  String name;
  int age;
  String gender;

  Human(this.age, this.name, this.gender);

  String returnName();
}

*/
/////////////////////////////////////////////////////////////////////////////////

/*
void main() {

/* 
В итоге, мы создали главный класс Human, у него есть child Student. 
Затем в каждом классе мы создали методы. 
Потом мы создали child у Student, к. назвали  FirstCourseStudent. 
Затем у FirstCourseStudent мы при создании метода смогли обращаться как к методам 
Student  так и методам Human. 
*/


  final shabdanbek =
      Student('Shabdanbek', 29, 'male', 'IT', 'group', [50, 60, 90]);
  print(shabdanbek.getAverage());

  Teacher emil =
      Teacher('Emil', 25, 'male', [100000, 300000, 800000], 'Flutter');

  // принтуем среднюю ЗП
  emil.printSalary();

  FirstCourseStudent dastan =
      FirstCourseStudent('Flutter', '2', [9, 10, 9, 10], 'Dastan', 40, 'male');

  // принтуем оценки через переопределнный метод getAverage
  // print(dastan.getAverage());

// принтуем среднюю оценку и имя

  print(dastan.getAverageAndAge());
}

class Human {
  String name;
  int age;
  String gender;

  Human(this.age, this.name, this.gender);

  String returnName() {
    return name;
  }
}

class Student extends Human {
  String group;
  String course;
  List grade;

  Student(
      String name, int age, String gender, this.course, this.group, this.grade)
      : super(age, name, gender);

  // создаем метод среднего балла через Вариант 2  с помощью   отдельного метода reduce

  dynamic getAverage() {
    return grade.reduce((value, element) => value += element) / grade.length;
  }

/*
  // создаем метод среднего балла  Вариант 1 через цикл 
double getAverage() {
    double sum = 0;
    for (int i = 0; i < grade.length; i++) {
      sum += grade[i];
    }
    return sum / grade.length;
  }
  // Составные операторы присваивания, такие как +=объединение операции с присваиванием.
  // Пример:	a += b >>> 	a = a + b

*/

}

// мы хотим   у Студента наследовать и создать новый класс, в к. создадим метод,
// к. будет возвращать среднюю оценку  и имя (имя берем у родителя Human в методе returnName ).
class FirstCourseStudent extends Student {
  FirstCourseStudent(String group, String course, List grade, String name,
      int age, String gender)
      : super(name, age, gender, course, group, grade);

//

  dynamic getAverageAndAge() {
    var a = super.getAverage() + age;

    return a.toString() + ' ' + super.returnName();
  }
}

class Teacher extends Human {
  List salary;
  String subject;

  //  содаем метод для средней ЗП
  double getAverageSalary() {
    return salary.reduce((value, element) => value += element) / salary.length;
  }

// создаем метод принт средней ЗП и его можно сразу у объекта вызывать
//  \$  этот знак обознает, что мы допишем $ к выводу принт
  void printSalary() {
    print('${getAverageSalary()} \$');
  }

  Teacher(String name, int age, String gender, this.salary, this.subject)
      : super(age, name, gender);
}
*/

///////////////////////////////////////////////////////////////////////////////////////

// String humanOneName = 'Shabdanbek';

// int humanHeght = 183;

// String humanTwoName = 'Max';

// чтобы как выше не мучаться, надо классы создавать и их экземпляров

/*

/////////////////////////////////////////////////////////////////////////////////
void main() {
  final shabdanbek = Student('Shabdanbek', 29, 'male', 'IT', 'l5');

// принт имени из заполненных данных
  print(shabdanbek.name);

  // принт метода isWork. Ответ: true. Т.е. child доступны все методы родителя.
  // А если переопределили, то будет возвращать false
  print(shabdanbek.isWork());

  final prepod = Human(45, 'Azamat Azamatovich', 'male');

  //  Здесь ответ будет всегда true при вызове метода, потому что мы вызываем метод у
  // Human, который является родителем, а его метод не переопределен у него самого же
  print(prepod.isWork());
}

class Human {
  String name;
  int age;
  String gender;

  bool isWork() {
    return true;
  }

  Human(this.age, this.name, this.gender);
  // это конструктор
}

// создаю наследование Student от Human

class Student extends Human {
  String group;
  String course;

// переопределили метод isWork у Human, и вместо true указали false.
  @override
  bool isWork() {
    return false;
  }

// здесь мы создаем конструктор для Student + добавляем параметры родителя Human
// через ключевое слово SUPER
  Student(String name, int age, String gender, this.course, this.group)
      : super(
            age, name, gender); // вот тут надо порядок такой как в конструкторе

}



*/
