import 'dart:io';

void main(List<String> arguments) {
/*  задача: 1 
Возьмем, например, два списка:
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
и напишите программу, которая возвращает список, содержащий только общие для них элементы (без дубликатов). Убедитесь, что ваша программа работает с двумя списками разного размера.
Ответ: [1, 2, 3, 5, 8, 13]
*/
  List<int> a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
  List<int> b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  List c = [];
  for (int i = 0; i < a.length; i++) {
    for (int j = 0; j < b.length; j++) {
      if (a[i] == (b[j])) {
        c.add(a[i]);
      }
    }
  }
  print(c.toSet().toList());
  /* задача: 2
Допустим, вам дан список, сохраненный в переменной:
а = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
Напишите код Dart, который берет этот список и создает новый список, в котором есть только четные элементы этого списка.
Ответ = [4, 16, 36, 64, 100];
  */
  List<int> a1 = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100];
  List<int> a1IsEven = [];
  int index = 0;
  while (index < a1.length) {
    if (a1[index].isEven) {
      a1IsEven.add(a1[index]);
    }
    index++;
  }
  print(a1IsEven);

  /*  задача:  
Создайте функцию, которая принимает две строки в качестве аргументов и возвращает, сколько раз первая строка (один символ) была найдена во второй строке.
Пример:
ввожу "a" и "dart" ➞ результат = 1
ввожу "c" и "Chamber of secrets" ➞ результат = 1
ввожу "b" и "big fat bubble" ➞ результат = 4
*/
  print('Введите одну любую букву');
  String firstLetter = stdin.readLineSync()!;

  print('Введите любое слово');
  String secondLetter = stdin.readLineSync()!;
  List list = secondLetter.split('');
  int count = 0;
  for (int i = 0; i < secondLetter.length; i++) {
    if (firstLetter == list[i]) {
      count++;
    }
  }
  print(count);
}




/* из классной работы do while 

____________________
c дз 
2-й вариант решения задачи:
Создайте функцию, которая принимает две строки в качестве аргументов и возвращает, сколько раз первая строка (один символ) была найдена во второй строке.
 
void main(List<String> arguments) {
  int count = 0;
  print('Введите одну любую букву');
  String firstLetter = stdin.readLineSync()!;

  print('Введите любое слово');
  String secondLetter = stdin.readLineSync()!;
  List list = secondLetter.split('');
  list.forEach((element) {
    if (element == firstLetter) {
      count++;
    }
  });
  print(count);
}
____________________
 /* задача: необходимо написать программу
1) Регистрация по логину и паролю
2) проверяет правильность введения пароля.
При введении правильного пароля необходимо вывести “Верный пароль”
При введении неверного пароля необходимо вывести “Повторите ввод ” и запрашивать ввод пароля 
 */

  print('Регистрация \n введите логин ');
  String userName = stdin.readLineSync()!;

  print('Введите пароль ');
  String password = stdin.readLineSync()!;
  print('Регистрация прошла успешно');
  print('введите пароль от логина $userName');
  bool result = false;
  do {
    String b = stdin.readLineSync()!;

    if (b == password) {
      result == true;
      print('верный пароль');
      break;
    } else {
      print('повторите ввод');
    }
  } while (result == false);
____________________
  // задача: вытащить e и положить его в массив, если он содержится в слове,
  String example = 'QWEReMRMMeER';
  List<int> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 16, 17, 13];
  List<String> correctNumbers = [];
  int index = 0;
  while (index < example.length) {
    if (example[index].toLowerCase() == 'e') { // toLowerCase помогает вытащить все e
      correctNumbers.add(example[index]);
    }
    index++;
  }
  print(correctNumbers);
____________________
  // задача: вытащить только те, к. делятся на 4 без остатка

  List<int> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 16, 17, 13];
  List<int> correctNumbers = [];

  int index = 0;
  while (index < numbers.length) {
    if (numbers[index] % 4 == 0) {  // поменял только здесь по сравнению с пред решением
                                    // (numbers[index] % 4 != 0) эта запись вернет все числа, к. не делятся на 4 без остатка
      correctNumbers.add(numbers[index]);
    }
    index++;
  }
  print(correctNumbers);
____________________
 // четное (isEven) или нечетное(isOdd) определить и положить в новый массив все четные
  // или все нечетные

  List<int> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 16, 17, 13];
  List<int> correctNumbers = [];

  int index = 0;
  while (index < numbers.length) {
    if (numbers[index].isOdd) {
      correctNumbers.add(numbers[index]);
    }
    index++;
  }
  print(correctNumbers);
____________________
// МЫ ПОМЕНЯЛИ УСЛОВИЯ ЗАДАЧИ, ЧТОБЫ ТЕПЕРЬ МЫ ПРИНТИЛИ ВСЕ ЭЛЕМЕНТЫ СО ЗНАКОМ ?
  int index = 0;
  List<String> data = ['Hello', 'Its me?', 'QWEQWE', 'Are you okay?'];

  List<String> questions = [];
  //  тип лист сделаем, чтобы ложить в него элементы
  while (index < data.length) {
    if (data[index].contains('?')) {
      questions.add(data[index]); // ложим элементы в лист
    }
    index++;
  }
  for (var element in questions) {
    print(element); // печатаем элементы листа
  }
____________________
 int index = 0;
  List<String> data = ['Hello', 'Its me?', 'QWEQWE', 'Are you okay?'];

  String questions = '';

  while (index < data.length) {
    // пока меньше чем длина массива, проверка будет срабатывать
    // но здесь мы будем получать всегда последний элемент массива со знаком ?
    // потому что предыдущие элементы цикл будет перезаписывать, пока не завершится. 
    //  и в итоге останется только последний присвоенный элемент со знаком ? - он и будет в принт
    if (data[index].contains('?')) {
      questions = data[index];
    }
    index++;
  }
  print(questions);
____________________
  int index = 0;
  List<String> data = ['Hello', 'Its me?', 'QWEQWE', 'Are you okay'];

  String questions = '';

  while (index < data.length) { // пока меньше чем длина массива, проверка будет срабатывать
    if (data[index].contains('?')) {
      questions = data[index];
    }
    index++;
  }
  print(questions);
}
____________________

задача: содержит ли массив элемент-строку с знаком вопроса '?'

  int index = -1; // поставил -1, чтобы иметь возможность увидеть '?' у элемента с индексом 0
  List<String> data = ['Hello', 'Its me?', 'QWEQWE', 'Are you okay?'];

  String questions = '';

  while (true) {   // условие будет постоянно выполняться, пока не найдет '?'
    index++;
    if (data[index].contains('?')) {
      // здесь беру элемент массива по индексу
      //  и проверяю содержание
      questions = data[
          index]; // найденный элемент-строку присваиваю переменной questions
      break;
    }
  }
  print(questions);
____________________
  int index = 0;

  while (index < 1) { // изначально равно 0, поэтому условие "меньше 1" срабатывает
  //  и к нулю плюсуем 1, потом делается принт 1, и дальше стоп -условие не проходит 
    index++;
    print(index);
  }
  // то что выше ниже написал через do while, НО код ниже сделает 
  //принт даже с условивем index < 0, а не index < 1 
  do {
    index++;
    print(index);
  } while (index < 0);   
____________________
  int index = 0;

  while (index < 1) { // изначально равно 0, поэтому условие "меньше 1" срабатывает
  //  и к нулю плюсуем 1, потом делается принт 1, и дальше стоп -условие не проходит 
    index++;
    print(index);
  }
____________________
  int index = 0;

  do {
    index++;
    print(index);
  } while (index < 15);  // принт index пока он меньше 15. старт с 1,
                          // так как 0 + 1 = 1
____________________
void main(List<String> arguments) {
  for (int i = 1; i <= 30; i++) {
    print(i);

    if (i == 15) {
      for (int j = 1; j <= 30; j++) {
        if (j == 15) {
          continue; // i работает до 15, потом стартует  j, но j не может запринтить 15,
          // так как эту операцию ограничил continue, после к. код продолжает работать со  знач 16
        }
        print(j);
      }
      break;
    }
  }
}
____________________
void main(List<String> arguments) {
  for (int i = 1; i < 30; i++) {
    print(i);

    if (i == 15) {
      for (int j = 1; j < 10; j++) {
        print(j);
      }
      break; // теперь я жестко прерываю первуя часть (принт i после 15 СТОП)
            // и затем вторая часть принтится с 1 до 9 (принт j)
    }
  }
}
____________________
void main(List<String> arguments) {
  for (int i = 1; i < 30; i++) {
    print(i); // здесь первый принт выполняется только до 15,
    // затем прерывается, и начинается с 16 до 29

    if (i == 15) {
      for (int j = 1; j < 10; j++) {
        print(j); // здесь у меня стартует новый принт c 1 до 9,
        // а потом снова продолжается первый принт
      }
    }
  }
}
____________________
void main(List<String> arguments) {
  int index = 0;

  while (index < 15) {
    print(index);  // еще один пример бесконченого цикла с помощью while 
  }
}

____________________
void main(List<String> arguments) {
  int i = 0; // переменную можно указать до цикла
  for (;;) {
    i++;
    print(i);  // если убрать breake то получается бесконечный цикл 
    // if (i == 100) {
    //   break;
    // }
  }
}

____________________
void main(List<String> arguments) {
  for (int i = 0; i <= 10; i++) {
    print(
        'квадрат числа $i = ${i * i}'); // обрати внимание на зпись выражения в скобках
    if (i == 5) {
      break; // прекращаю дейтсвие на 5, хотя в условии писал 10
    }
  }
}


______________
*/
